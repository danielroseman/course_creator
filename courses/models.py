from django.db import models

class Course(models.Model):
  title = models.CharField(max_length=255)
  description = models.TextField()
  total_duration = models.IntegerField()

  def __unicode__(self):
    return self.title


class Module(models.Model):
  course = models.ForeignKey(Course)
  text_content = models.TextField()
  interval = models.IntegerField(default=1)
  order = models.IntegerField()

  def __unicode__(self):
    return "Module {} of course {}".format(self.order, self.course.title)

USER_TYPES = (
    (1, 'Teacher'),
    (2, 'Student'),
)

class Enrollment(models.Model):
  course = models.ForeignKey(Course)
  user = models.ForeignKey('auth.User')
  user_type = models.IntegerField(choices=USER_TYPES)
  start_date = models.DateField(auto_now_add=True)
  email_time = models.TimeField()
  last_module = models.ForeignKey(Module, blank=True, null=True)

  def __unicode__(self):
    return "User {}'s enrollment in {}".format(self.user.username, self.course.title)
