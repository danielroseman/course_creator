# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Enrollment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_type', models.IntegerField(choices=[(1, b'Teacher'), (2, b'Student')])),
                ('start_date', models.DateField(auto_now_add=True)),
                ('email_time', models.TimeField()),
                ('course', models.ForeignKey(to='courses.Course')),
                ('last_module', models.ForeignKey(to='courses.Module')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
