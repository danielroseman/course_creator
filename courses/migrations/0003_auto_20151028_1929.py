# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0002_enrollment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enrollment',
            name='last_module',
            field=models.ForeignKey(blank=True, to='courses.Module', null=True),
        ),
    ]
