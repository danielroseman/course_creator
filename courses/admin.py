from django.contrib import admin
from courses.models import Course, Module, Enrollment

class ModuleInlineAdmin(admin.TabularInline):
  model = Module

class CourseAdmin(admin.ModelAdmin):
  model = Course
  inlines = [ModuleInlineAdmin]

admin.site.register(Course, CourseAdmin)
admin.site.register(Enrollment)

